# MediaWiki Page Readability by Category

A small [Slim][slim] application for returning a list of pages in a MediaWiki
category, ordered by [Dale-Chall readability scores][dale-chall].

Works in [Lynx][lynx], for some reason, as well as browsers that people
actually use.

Relies on [Dave Child's TextStatistics][textstats] for readability scores.

## Installing

First, have Composer, npm, and PHP plus some common extensions.

On a Debian system (Jessie or better):

```
sudo apt-get install php-cli php-xml php-mbstring php-bcmath php-curl
git clone https://gitlab.com/brennenpike/mw-category-readability.git
cd mw-category-readability
npm install
composer install
composer start
```

And then visit [http://localhost:8080](http://localhost:8080) in your browser
of choice.

## Hypothetical TODOs

  - Pagination
  - Page previews
  - Any hint of visual design

[textstats]: https://github.com/DaveChild/Text-Statistics
[lynx]: http://lynx.invisible-island.net/
[slim]: https://www.slimframework.com
[dale-chall]: https://en.wikipedia.org/wiki/Dale%E2%80%93Chall_readability_formula
